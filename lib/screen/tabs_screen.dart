import 'package:flutter/material.dart';
import 'package:restaurant_menu_app/screen/category_screen.dart';
import 'package:restaurant_menu_app/screen/favorites_screen.dart';
import 'package:restaurant_menu_app/widget/main_drawer.dart';

class TabsScreen extends StatefulWidget {
  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  final List<Map<String, Object>> _listPage = [
    {
      'page': CategoryScreen(),
      'title': 'Categories',
    },
    {
      'page': FavoritesScreen(),
      'title': 'Favorites',
    },
  ];
  int _selectedPageIndex = 0;

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          _listPage[_selectedPageIndex]['title'],
        ),
      ),
      drawer: MainDrawer(),
      body: _listPage[_selectedPageIndex]['page'],
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        elevation: 4,
        iconSize: 24,
        backgroundColor: Theme.of(context).primaryColor,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.yellow,
        selectedFontSize: 24,
        unselectedFontSize: 22,
        currentIndex: _selectedPageIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.category,
            ),
            title: Text(
              'Categories',
              style: TextStyle(fontSize: 16),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.star,
            ),
            title: Text(
              'Favorites',
              style: TextStyle(fontSize: 16),
            ),
          ),
        ],
      ),
    );
  }
}
