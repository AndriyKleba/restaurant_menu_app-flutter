import 'package:flutter/material.dart';
import 'package:restaurant_menu_app/screen/category_screen.dart';
import 'package:restaurant_menu_app/screen/category_meals_screen.dart';
import 'package:restaurant_menu_app/screen/meal_detail_screen.dart';
import 'package:restaurant_menu_app/screen/tabs_screen.dart';
import 'package:restaurant_menu_app/screen/settings_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DeliMeals',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        accentColor: Colors.amber,
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
              bodyText1: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              bodyText2: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              headline1: TextStyle(
                fontSize: 24.0,
                fontFamily: 'RobotoCondenced',
                fontWeight: FontWeight.w700,
              ),
            ),
      ),
      routes: {
        '/': (ctx) => TabsScreen(),
        CategoryMealsScreen.routsName: (ctx) => CategoryMealsScreen(),
        MealDetailScreen.routsName: (ctx) => MealDetailScreen(),
        SettingsScreen.routsName: (ctx) => SettingsScreen(),
      },
      onUnknownRoute: (settings) {
        return MaterialPageRoute(
          builder: (ctx) => CategoryScreen(),
        );
      },
    );
  }
}
